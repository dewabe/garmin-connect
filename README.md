# garmin-connect

Käytössäni oleva nopea viritelmä (lue: ei optimoitu millään tavalla), jolla näen (toivottavasti lähiaikoina) valitulta ajanjaksolta millaisilla sykealueilla on tullut treenattua, miten syke, tahti ja "etenemä" on kehittynyt ajan kuluessa. Skripta käyttää https://github.com/dawguk/php-garmin-connect. 

**update.php** on tarkoitettu vain tiedon hakuun ja arkistointiin. Tarkista tietokannan ja Garmin Connectin kirjautumistiedot oikeaksi.

Tästä tiedostosta löytyy myös HRZones-funktio, jonka perusteella jokaisen treenin syketiedot laitetaan tietokantaan ("zones" tauluun).

Skripta hakee Garminista viisi viimeisintä treeniä ja lisää kaikki, mitkä eivät vielä ole jo kannassa (vertailuna id-numero), rivi 111
**$activities = $objGarminConnect->getActivityList(0, 5, 'running');**

Jos haluat hakea kaikki historiatietosi talteen, suosittelen että haet tietoja pienissä pätkissä: muuten tulee aikakatkaisu vastaan. Kannattaa myös huomioida se, että getActivityList-metodille annetaan "aloituskohta" (tässä 0) ja kuinka monta treeniä haetaan (tässä 5). Eli jos haluat hakea tuosta seuraavat viisi, laita 0 tilalle 5: **getActivityList(5, 5, 'running');**, ja taas tämän jälkeen seuraavat viisi: **getActivityList(10, 5, 'running');**

Muutaman kymmenen voi hakea kerralla vielä ihan hyvinkin.

Skripta hakee myös päivittäisen leposykkeen, jota käytän "etenemä"-arvossa.

**index.php** näytetään itse dataa. Käyttää mm. Chart.js-kirjastoa. Muista muuttaa myös tänne tietokannan kirjautumistiedot. Tarkista myös sykealueet itsellesi sopiviksi (ainakin rivit 66 ja 235), jotta graaffissa näkyy tiedot oikein.

## Tietokanta

Tietokannan nimi: MyTrainings

CREATE TABLE `daily` (
    `date` date NOT NULL,
    `hr_rest` int(11) NOT NULL,
    UNIQUE KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `trainings` (
    `id` bigint(11) NOT NULL,
    `date` date NOT NULL,
    `distance` int(11) NOT NULL,
    `duration` int(11) NOT NULL,
    `hr_avg` int(11) NOT NULL,
    `hr_max` int(11) NOT NULL,
    `name` text NOT NULL,
    `tread` double NOT NULL,
    UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `zones` (
    `id` bigint(11) NOT NULL,
    `zone1` int(11) NOT NULL,
    `zone2` int(11) NOT NULL,
    `zone3` int(11) NOT NULL,
    `zone4` int(11) NOT NULL,
    `zone5` int(11) NOT NULL,
    `zone6` int(11) NOT NULL,
    `date` date DEFAULT NULL,
    UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;