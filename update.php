<?php
#ini_set('display_errors', 1);

require_once('dawguk/GarminConnect.php');
require_once('dawguk/GarminConnect/Connector.php');
use dawguk\GarminConnect;

$arrCredentials = array(
   'username' => 'GARMIN_USERNAME',
   'password' => 'GARMIN_PASSWORD',
);

$servername = "localhost";
$username = "DB_USERNAME";
$password = "DB_PASSWORD";
$dbname = "myTrainings";

function getMetricsId($metricDescriptions, $key='directHeartRate') {
    foreach($metricDescriptions as $metricDescription) {
        if($metricDescription->key == $key)
            return $metricDescription->metricsIndex;
    }
}

function HRZones() {
    return [
        "Zone1" => [
            "min" => 0,
            "max" => 130,
            "value" => 0,
            "color" => "rgba(220, 220, 220, 0.2)"
        ],
        "Zone2" => [
            "min" => 131,
            "max" => 145,
            "value" => 0,
            "color" => "rgba(220, 220, 220, 0.3)"
        ],
        "Zone3" => [
            "min" => 146,
            "max" => 156,
            "value" => 0,
            "color" => "rgba(100, 200, 100, 0.3)"
        ],
        "Zone4" => [
            "min" => 157,
            "max" => 167,
            "value" => 0,
            "color" => "rgba(230, 230, 50, 0.3)"
        ],
        "Zone5" => [
            "min" => 168,
            "max" => 177,
            "value" => 0,
            "color" => "rgba(240, 0, 0, 0.2)"
        ],
        "Zone6" => [
            "min" => 178,
            "max" => 192,
            "value" => 0,
            "color" => "rgba(255, 0, 0, 1)"
        ]
    ];
}

function HRTimeInZones($result, $hr_id) {
    $HRTimeInZones = HRZones();

    $duration_id = getMetricsId($result->metricDescriptors, $key='sumElapsedDuration');
    $previous_duration = 0;

    foreach($result->activityDetailMetrics as $metric) {
        $hr = $metric->metrics[$hr_id];
        $duration = $metric->metrics[$duration_id] - $previous_duration;
        foreach($HRTimeInZones as &$zone) {
            if($zone['min'] <= $hr && $hr <= $zone['max'])
                $zone['value'] += $duration;
        }
        $previous_duration = $metric->metrics[$duration_id];
        
    }
    return $HRTimeInZones;
}


//$ActivityID = isset($_GET['activity_id']) ? $_GET['activity_id'] : '';

try {
    $objGarminConnect = new GarminConnect($arrCredentials);

    /*
    WORKFLOW
    * Get 10 last exercises -> getActivityList(0, 10, 'running')
    * Check one by one does it exists already -> SELECT id FROM trainings WHERE id=id
    * If new records available, add all one by one
    ** Required rest heartrate of current date -> getWellnessData(this_date, this_date)['allMetrics']['metricsMap']['WELLNESS_RESTING_HEART_RATE']
    ** Add this information to table 'daily': date, hr_rest
    ** Add basic information to table 'trainings': id, date, distance, duration, averageHR, maxHR, name, tread
    ** Formula for tread:   distance [in meters] / ((averageHR - hr_rest) * duration [in minutes])
    ** Get training details -> getExtendedActivityDetails(training_id)
    ** Generate zones and add to table 'zones': date, zone1, zone2, zone3, zone4, zone5, zone6
    */

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    // Get 5 last exercises
    $activities = $objGarminConnect->getActivityList(0, 5, 'running');
    // Check one by one does it exists already
    foreach($activities as $activity) {
        $sql = 'SELECT id FROM trainings WHERE id="' . $activity->activityId . '"';
        //$result = $conn->query($sql);
        if ($conn->query($sql)->num_rows == 0) {
            // New record available...
            // getWellnessDate
            $this_date = explode(' ', $activity->startTimeLocal)[0];
            $hr_rest = $objGarminConnect->getWellnessData($this_date, $this_date)['allMetrics']['metricsMap']['WELLNESS_RESTING_HEART_RATE'][0]['value'];
            
            $sql_daily = "INSERT INTO daily (date, hr_rest) VALUES ('" . $this_date . "', '" . $hr_rest . "')";
            $conn->query($sql_daily);

            // Add basic information to table 'trainings'
            $tread = ($activity->distance) / (($activity->averageHR - $hr_rest) * ($activity->duration / 60));
            $sql_trainings = "INSERT INTO trainings (id, date, distance, duration, hr_avg, hr_max, name, tread)
            VALUES (
                '". $activity->activityId ."',
                '". $this_date ."',
                '". intval($activity->distance) ."',
                '". intval($activity->duration) ."',
                '". intval($activity->averageHR) ."',
                '". intval($activity->maxHR) ."',
                '". $activity->activityName ."',
                '". $tread ."'
            )";
            $conn->query($sql_trainings);

            // Generate zones and add to table 'zones'
            $extended_activity = $objGarminConnect->getExtendedActivityDetails($activity->activityId);
            $hr_id = getMetricsId($extended_activity->metricDescriptors);
            $zones = HRTimeInZones($extended_activity, $hr_id);

            $sql_zones = "INSERT INTO zones (id, zone1, zone2, zone3, zone4, zone5, zone6, date)
            VALUES (
                '". $activity->activityId ."', 
                '". $zones['Zone1']['value'] ."', 
                '". $zones['Zone2']['value'] ."', 
                '". $zones['Zone3']['value'] ."', 
                '". $zones['Zone4']['value'] ."', 
                '". $zones['Zone5']['value'] ."', 
                '". $zones['Zone6']['value'] ."',
                '". $this_date . "'
            )";
            $conn->query($sql_zones);
        }

    }


    $conn->close();
}

catch (Exception $objException) {
   echo "Oops: ";
}

?>