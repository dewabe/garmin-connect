<?php
$get_id = isset($_GET['id']) ? intval($_GET['id']) : '';
$action = isset($_GET['action']) ? $_GET['action'] : '';

$servername = "localhost";
$username = "DB_USERNAME";
$password = "DB_PASSWORD";
$dbname = "myTrainings";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>
<html>
<head>
<title>Yhteenveto</title>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<style>
html body {

}

.chart-container {
    max-width: 800px;
    position: relative;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<?php

if(!$action) {
    ?>
Treenilista <a href="?action=activities">täältä</a>.<br />
<div class="chart-container">
    <canvas id="myChart"></canvas>
</div>

<div class="chart-container">
    <canvas id="myChart2"></canvas>
</div>

<div class="chart-container">
    <canvas id="myChart3"></canvas>
</div>

<div class="chart-container">
    <canvas id="myChart4"></canvas>
</div>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: ['<130', '131-145', '146-156', '157-167', '168-177', '178<'],
        datasets: [{
            label: 'Aika alueella (s)',
            data: [<?php

            $sql = "SELECT SUM(zone1) as z1, SUM(zone2) as z2, SUM(zone3) as z3, SUM(zone4) as z4, SUM(zone5) as z5, SUM(zone6) as z6 FROM zones";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo $row["z1"]. ", " . $row["z2"]. ", " . $row["z3"]. ", " . $row["z4"]. ", " . $row["z5"]. ", " . $row["z6"];
                }
            }
            ?>],
            backgroundColor: [
                'rgba(220, 220, 220, 0.5)',
                'rgba(150, 150, 150, 0.5)',
                'rgba(100, 100, 250, 0.5)',
                'rgba(100, 200, 100, 0.5)',
                'rgba(230, 230, 50, 0.5)',
                'rgba(240, 0, 0, 0.5)',
            ],
            borderWidth: 1
        }],
    },
    options: {
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                //get the concerned dataset
                var dataset = data.datasets[tooltipItem.datasetIndex];
                //calculate the total of this data set
                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                });
                //get the current items value
                var currentValue = dataset.data[tooltipItem.index];
                //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                return (new Date(currentValue * 1000).toISOString().substr(11, 8)) + " | " + percentage + " %";
                }
            }
} 
    }
});

<?php
$sql = "SELECT WEEK(date, 3) as dt, AVG(tread) as etenema FROM trainings GROUP BY WEEK(date, 3)  ORDER BY date";

$result = $conn->query($sql);

$labels = '';
$data = '';

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $labels .= "'" . $row["dt"] . "', ";
        $data .= $row["etenema"] . ", ";
    }
}
?>
var ctx2 = document.getElementById('myChart2').getContext('2d');
var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: [<?php echo substr($labels, 0, -2); ?>],
        datasets: [{
            label: 'Etenemä',
            data: [<?php echo substr($data, 0, -2); ?>],
            borderWidth: 1
        }]
    }
});

<?php
$sql = "SELECT WEEK(date, 3) as dt,
(AVG(duration) / 60) / (AVG(distance) / 1000) as pace FROM trainings GROUP BY WEEK(date, 3) ORDER BY date";

$result = $conn->query($sql);

$labels = '';
$data = '';

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $labels .= "'" . $row["dt"] . "', ";
        $data .= $row["pace"] . ", ";
    }
}
?>
var ctx3 = document.getElementById('myChart3').getContext('2d');
var myChart3 = new Chart(ctx3, {
    type: 'line',
    data: {
        labels: [<?php echo substr($labels, 0, -2); ?>],
        datasets: [{
            label: 'Tahti',
            data: [<?php echo substr($data, 0, -2); ?>],
            borderWidth: 1
        }]
    }
});

<?php
$sql = "SELECT WEEK(date, 3) as dt, AVG(hr_avg) as hr FROM trainings GROUP BY WEEK(date, 3) ORDER BY date";

$result = $conn->query($sql);

$labels = '';
$data = '';

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $labels .= "'" . $row["dt"] . "', ";
        $data .= $row["hr"] . ", ";
    }
}
?>
var ctx4 = document.getElementById('myChart4').getContext('2d');
var myChart4 = new Chart(ctx4, {
    type: 'line',
    data: {
        labels: [<?php echo substr($labels, 0, -2); ?>],
        datasets: [{
            label: 'Syke',
            data: [<?php echo substr($data, 0, -2); ?>],
            borderWidth: 1
        }]
    }
});
</script>
<?php
    }

elseif($get_id && $action == "activity") {
?>
<?php

$sql = "SELECT * FROM trainings INNER JOIN daily ON daily.date = trainings.date  WHERE trainings.id = '".$get_id."'";
$result = $conn->query($sql)->fetch_assoc();

?>
<h1><?php echo $result["name"]; ?></h1>
Matka: <?php echo round($result["distance"] / 1000, 1); ?> km<br />
Kesto: <?php
    echo gmdate("H:i:s", $result["duration"] );
?> <br />
Keskitahti: <?php
echo date('i:s', (($result["duration"] ) / ($result["distance"] / 1000)) );
?> min/km<br />
Keskisyke: <?php echo $result["hr_avg"]; ?> bpm<br />
Maksimisyke: <?php echo $result["hr_max"]; ?> bpm<br />
Etenemä: <?php echo round($result["tread"], 2); ?> m/b<br />
Leposyke: <?php echo $result["hr_rest"]; ?> bpm
<div class="chart-container">
    <canvas id="myChart"></canvas>
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['<130', '131-145', '146-156', '157-167', '168-177', '178<'],
        datasets: [{
            label: 'Aika alueella (s)',
            data: [<?php

            $sql = "SELECT SUM(zone1) as z1, SUM(zone2) as z2, SUM(zone3) as z3, SUM(zone4) as z4, SUM(zone5) as z5, SUM(zone6) as z6 FROM zones WHERE id = '".$get_id."'";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    echo $row["z1"]. ", " . $row["z2"]. ", " . $row["z3"]. ", " . $row["z4"]. ", " . $row["z5"]. ", " . $row["z6"];
                }
            }
            ?>],
            backgroundColor: [
                'rgba(220, 220, 220, 0.5)',
                'rgba(150, 150, 150, 0.5)',
                'rgba(100, 100, 250, 0.5)',
                'rgba(100, 200, 100, 0.5)',
                'rgba(230, 230, 50, 0.5)',
                'rgba(240, 0, 0, 0.5)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
<?php
}

elseif($action=="activities") {
    ?>
<style>
#trainings {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#trainings td, #trainings th {
  border: 1px solid #ddd;
  padding: 8px;
}

#trainings tr:nth-child(even){background-color: #f2f2f2;}

#trainings tr:hover {background-color: #ddd;}

#trainings th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<table id="trainings">
    <tr>
        <th>Päivämäärä</th>
        <th>Nimi</th>
        <th>Matka (km)</th>
        <th>Kesto (hh:mm:ss)</th>
    </tr>
    <?php
    $sql = "SELECT id, name, date, duration, distance FROM trainings ORDER BY date DESC";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            ?>
    <tr class="clickable-row" data-href='?action=activity&id=<?php echo $row['id']; ?>'>
        <td><?php echo $row["date"]; ?></td>
        <td><?php echo $row["name"]; ?></td>
        <td><?php echo round($row["distance"] / 1000, 1); ?></td>
        <td><?php echo gmdate("H:i:s", $row["duration"]); ?></td>
    </tr>
            <?php
        }
        ?>
    </table>
<script>
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>
        <?php
    }
}
?>
</body>
</html>
<?php
$conn->close();
?>